const axios = require('axios');
const { sendMail } = require('../mail/nodmaile');
const { logger } = require('../logger/logger');

const sendToWebHook = ({ url, data }) => axios.post(url, { ...data })
  .then((res) => logger.info(`sent to webhook : ${url}`))
  .catch((err) => logger.error(err));

const notificationMeans = {
  mail: sendMail,
  webHook: sendToWebHook,
};

module.exports = notificationMeans;
