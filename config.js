const config = {
  port: process.env.PORT,
  DBUrl: process.env.MONGO_URL,
  mailUsername: process.env.MAIL_USER_NAME || 'amir.haytham.salama@gmail.com',
  mailPassword: process.env.MAIL_PASSWORD || '123456', // Passwords Should be imported as secret and not written in plain text
};

module.exports = config;
