# URL monitoring server
bosta task

## Introduction
The main idea of the task is to build an uptime monitoring RESTful API server which allows authorized users to enter URLs they want monitored, and get detailed uptime reports about their availability, average response time, and total uptime/downtime.

## Application Guide:

1. "git clone" repository on your device
```
  $ git clone https://gitlab.com/AmirHaytham/bosta.git
  $ cd bosta
```
2. run "docker-compose up -d" inside cloned repository
  ```bash
  $ docker-compose up
  ```
3. Server will run by default on port `3000`, and is accessible from `http://localhost:3000`
4. navigate to swagger from `http://localhost:3000/explorer`
5. Database will be accessible from `http://localhost:27018`

## Why Node Js?
There are many concurrent requests that hits the server, the requests are mainly I/O operations which do not require many computational power that may block the event loop. For these two reasons, I have chosen node js as node is very light weight and execute fast. Also, node works with the event loop which is single threaded that preforms all I/O operations asynchronously without blocking the remaning code, this make it preform very fast in handling too many requests at the same time.

## Why I choose NoSql?
NoSQL databases (aka "not only SQL") are non-tabular databases and store data differently than relational tables. NoSQL databases come in a variety of types based on their data model. The main types are document, key-value, wide-column, and graph. They provide flexible schemas and scale easily with large amounts of data and high user loads.


## Project Structure:
- Nodejs-server is the node application
- Config directory -> catch exported environment variables to be used for project setup
- Database directory -> setup "mongoose" orm
- Errors directory -> defining errors needed in the project
- Models directory -> database models
- Server directory -> the source code for the project
- Swagger (OpenAPI) -> API documentation

# Main Features
- Fundamental of Express: routing, middleware, sending response and more
- Fundamental of Mongoose: Data models, data validation and middleware
- RESTful API including pagination and sorting
- CRUD operations with MongoDB
- Security: encyption, sanitization and more
- Authentication with JWT: login and signup
- Authorization (User roles and permissions)
- Error handling
- Enviroment Varaibles
- handling error outside Express
- Catching Uncaught Exception
- Sign-up with email verification.
- Stateless authentication using JWT.
- Users can create a check to monitor a given URL if it is up or down.
- Users can edit, pause, or delete their checks if needed.
- Users may receive a notification on a webhook URL by sending HTTP POST request whenever a check goes down or up.
- Users should receive email alerts whenever a check goes down or up.
- Users can get detailed uptime reports about their checks availability, average response time, and total uptime/downtime.
- Users can group their checks by tags and get reports by tag.

# Main Features in a big picture
* [Auth](#auth)
  * [Login](#1-login)
  * [Sign up](#2-sign-up)
  * [Verify Email | Sending Confirmation](#3-verify-email-token)
--------
* [Checks](#checks)
  * [Create Check](#1-create-check)
  * [Delete Check](#2-delete-check)
  * [Edit Check Status](#3-edit-check-status)
  * [Get Checks By Tag](#4-get-checks-by-tag)
  * [Get Checks For User](#5-get-checks-for-user)
  * [Get Reports By Tag](#6-get-reports-by-tag)
  * [Get Single Check](#7-get-single-check)
  * [Perform Checks](#8-perform-checks)
--------
* [Reports](#reports)
  * [Get Rports for User](#1-get-reports-for-user)
