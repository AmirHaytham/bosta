module.exports = {
  openapi: '3.0.0',
  info: {
    title: 'bosta',
    version: '1.0.0',
    description:
      'A REST API server made with Express framework. It monitors url that users want to trigger',
    license: {
      name: 'Licensed Under MIT',
      url: 'https://spdx.org/licenses/MIT.html',
    },
    contact: {
      name: 'JSONPlaceholder',
      url: 'https://jsonplaceholder.typicode.com',
    },
  },
  servers: [
    {
      url: `http://localhost:${process.env.PORT || 3000}`,
      description: 'dev server',
    },
  ],
};
